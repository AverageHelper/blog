# Average Blog

A simple blog with only respectful frills, generated using [Zola](https://getzola.org), and hosted on [Codeberg Pages](https://codeberg.page) (via Git [mirror](https://codeberg.org/AverageHelper/blog)).

## Features

- Categories
- Tags
- Search
- Estimated minutes to read each article

## Prerequisites

Follow the [Zola Docs](https://www.getzola.org/documentation/getting-started/installation/) to complete the installation of the `Zola` site generator for your system.

## Usage

### Running Local Server

```console
$ npm start
```

Open a browser and visit _<http://127.0.0.1:1111>_.

### Deployment

```console
$ npm run build
```

Deploy the contents of the `public` directory as you see fit.

## Contributing

The old saying, "Two heads are better than one." Consequently, welcome to report bugs, improve code quality or submit a new feature.

This project lives primarily at [git.average.name](https://git.average.name/AverageHelper/blog). A read-only mirror also exists on [Codeberg](https://codeberg.org/AverageHelper/blog). Issues or pull requests should be filed at [git.average.name](https://git.average.name/AverageHelper/blog). You may sign in or create an account directly, or use one of several OAuth 2.0 providers.

## License

Except where otherwise noted, the blog posts on this site are licensed under the [Creative Commons Attribution 4.0 International (CC BY 4.0)](https://creativecommons.org/licenses/by/4.0/) License by the author.
