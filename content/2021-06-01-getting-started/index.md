+++
title = "Getting Started"
author = "Madeline Robinson"
# description = "My very first blog post."
date = 2021-06-01T10:34:00-06:00
draft = false

[taxonomies]
categories = ["Blogging"]
tags = ["first", "starting out", "beginning", "blogging"]

[extra]
toc = true
keywords = "Blogging, Markdown, Jekyll"
# thumbnail = "ferris-gesture.png"
+++

First!

This it literally my first-ever blog post. It ain't much, but it's something.

I expect I'll make posts on here from time to time with regards to my open-source work, some software projects I'm paid for, and other subjects as I get time and inclination to write.

In short, don't expect much. I'm just some guy, ya know?
